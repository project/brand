<?php

/**
 * @file
 * Include file for all forms functions.
 */

/**
 * Get an associative array of terms.
 *
 * @return array
 *   An associative array of terms.
 */
function brand_form_get_terms() {
  // Get and store a key => value structured array with
  // the VID containing the term name and term id.
  $vocabularies = taxonomy_get_vocabularies();
  $results = array();
  $results['Unselected'][0] = 'Not selected';
  foreach ((array) $vocabularies as $vocabulary) {
    $terms = taxonomy_get_tree($vocabulary->vid);
    foreach ($terms as $term) {
      $results[$vocabulary->name][$term->tid] = $term->name;
    }
  }
  return $results;
}

/**
 * Cancel button for form.
 */
function _brand_form_cancel() {
  $arguments = explode('/', $_GET['q']);
  $machine_name = $arguments[4];
  if (count($arguments) === 5 && ($arguments[3] == 'brands') && ($arguments[4] == 'add')) {
    drupal_goto('/admin/config/user-interface/brands/');
    return;
  }
  if (is_numeric((int) $arguments[5])) {
    $timestamp = (int) $arguments[5];
  }
  if (isset($timestamp) && isset($machine_name)) {
    drupal_goto("/admin/config/user-interface/brands/{$machine_name}/");
    return;
  }
  elseif (isset($machine_name)) {
    drupal_goto("/admin/config/user-interface/brands/{$machine_name}");
    return;
  }
}

/**
 * Returns a standardized $form object.
 *
 * To be used as a parameter to array_merge().
 * This may or may not include options , values
 * or default values for each field.
 */
function _brand_get_universal_form() {
  global $user;
  $form = array();
  $theme_restrictions = (int) variable_get('brand_disable_checking', 0);
  if ($theme_restrictions === 1) {
    $all_themes = list_themes();
    $themes = array();
    foreach ($all_themes as $name => $theme) {
      if ((int) $theme->status === 1) {
        $themes[$name] = $name;
      }
    }
  }
  elseif ($theme_restrictions === 0) {
    $all_themes = brand_get_allowed_themes();
    $drupal_themes = list_themes();
    $themes = array();
    foreach ($all_themes as $name => $theme) {
      if ((int) $drupal_themes[$theme]->status === 1) {
        $themes[$theme] = $theme;
      }
    }
  }

  if (module_exists('book')) {
    $book_raw = book_get_books();
    $books = array();
    foreach ($book_raw as $key => $value) {
      $books[$key] = $book_raw[$key]['title'];
    }
  }

  /* Declare the vertical tab groups. */

  $form['vtabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#group' => 'vtabs',
  );

  $form['assets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Assets'),
    '#group' => 'vtabs',
  );

  $form['dates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dates'),
    '#group' => 'vtabs',
  );

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type'),
    '#group' => 'vtabs',
  );

  if (module_exists('book')) {
    $form['book'] = array(
      '#type' => 'fieldset',
      '#title' => t('Book'),
      '#group' => 'vtabs',
    );
  }

  $form['role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role'),
    '#group' => 'vtabs',
  );

  if (module_exists('taxonomy')) {
    // @TODO: Taxonomy support is currently broken.
    // See https://www.drupal.org/project/brand/issues/2955745
    // for more information. It's currently hidden from view.
    $form['taxonomy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy'),
      '#group' => 'vtabs',
      '#access' => TRUE,
    );
  }

  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#group' => 'vtabs',
  );

  /* Declare fields. */

  $form['basic']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Branding name'),
    '#description' => t("This is the human-readable name associated to the brand.<br />It's only used on administration pages to identify the brand."),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['basic']['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#description' => t('The name which Drupal will associate to this brand.<br />This value must be unique, as it is used to discover if the brand should be displayed and to show information in the administration pages.'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['basic']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('The message associated to this change, which could include details about what changed, who asked for the changes and why the changes are occuring.'),
    '#default_value' => "Created by {$user->name}",
    '#rows' => 5,
    '#cols' => 5,
    '#maxlength' => 500,
    '#wysiwyg' => FALSE,
    '#format' => 'plain_text',
    '#required' => TRUE,
  );

  $restriction = variable_get('brand_disable_checking', 0);
  if ((int) $restriction === 0) {
    $form['assets']['theme_description'] = array(
      '#type' => 'markup',
      '#markup' => '<p>If the desired theme, or no theme is available, please contact your administrator to either open up access to specific themes or to remove restrictions based upon the themes you are allowed to choose. Any selected themes will cease to work in the event your administrator imposes changes which deem the selected theme to be unavailable.</p><p>In addition to this, the list of available themes are also limited to enabled themes - you may wish to speak to your administrator about this in the event you cannot find the theme you need.</p>',
    );
  }
  else {
    $form['assets']['theme_description'] = array(
      '#type' => 'markup',
      '#markup' => '<p>Your administrator has not imposed theme-specific restrictions to the brand module for this website. The list of available themes is only limited to enabled themes - you may wish to speak to your administrator about this in the event you cannot find the theme you need.</p>',
    );
  }

  $form['assets']['theme'] = array(
    '#type' => 'radios',
    '#title' => 'Theme',
    '#description' => t('Which theme do you wish to associate to this brand? If no themes are showing, contact your administrator.'),
    '#required' => FALSE,
    '#options' => $themes,
  );

  $form['assets']['weight'] = array(
    '#type' => 'select',
    '#title' => 'Weight',
    '#description' => t('Specify a weight, which will be used to determine if a particular brand should rule out including conflicting brands.'),
    '#required' => FALSE,
  );
  $options = array();
  for ($i = -20; $i <= 20; $i++) {
    $options[(int) $i] = $i;
  };
  $form['assets']['weight']['#options'] = $options;
  $form['assets']['weight']['#default_value'] = 0;

  $date = new DateTime();
  $now = $date->getTimestamp();

  $form['dates']['date_created'] = array(
    '#type' => 'textfield',
    '#title' => t('Creation date'),
    '#description' => t('The timestamp associated to this entry, be it the first entry of a new brand or any subsequent changes.<br />This value represents the timestamp for when this form was generated.'),
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#access' => FALSE,
    '#default_value' => $now,
  );

  $form['dates']['date_start'] = array(
    '#type' => 'date',
    '#title' => t('Start date'),
    '#description' => t('The date in which this brand will become available.<br /><strong>Note</strong>: This will be used in the UNIX timestamp format based upon the configured server time.<br /><strong>Note</strong>: Caches could still prevent the change from happening when expected.'),
    '#required' => TRUE,
  );

  $form['dates']['date_finish'] = array(
    '#type' => 'date',
    '#title' => t('Finish date'),
    '#description' => t('The date in which this brand will stop being available.<br /><strong>Note</strong>: This will be used in the UNIX timestamp format based upon the configured server time.<br /><strong>Note</strong>: Caches could still prevent the change from happening when expected.'),
    '#required' => FALSE,
  );

  $form['dates']['date_lock'] = array(
    '#type' => 'checkbox',
    '#title' => 'Prevent brand expiration',
    '#description' => t('Instead of stopping the availability of a brand, you can continue it indefinitely (until configured otherwise) by checking this option.'),
    '#default_value' => 1,
  );

  $types = node_type_get_types();
  $content_types = array();
  foreach ($types as $type) {
    $content_types[$type->type] = $type->type;
  }

  $form['types']['content_types'] = array(
    '#title' => 'Content types',
    '#type' => 'checkboxes',
    '#description' => t('Select which content types to apply this Brand to.'),
    '#options' => $content_types,
  );

  if (module_exists('book')) {
    $form['book']['books'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Books'),
      '#description' => t('Select which books to apply this Brand to.'),
      '#options' => $books,
    );
  }

  $form['role']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => 'User access',
    '#description' => t('Select which roles to apply this Brand to.<br />If no role is selected, all roles will be able to see the theme.'),
    '#options' => user_roles(),
  );

  if (module_exists('taxonomy')) {

    $form['taxonomy']['term'] = array(
      '#type' => 'select',
      '#title' => 'Term',
      '#description' => t('All terms will be shown here<br />Select a term to target all content which is tagged with the given term.'),
      '#prefix' => '<div id="view-display-dropdown">',
      '#suffix' => '</div>',
      '#options' => brand_form_get_terms(),
      '#default_value' => NULL,
    );
  }

  $form['visibility']['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t('Manually enter one or more paths (optionally using globs) to show the branding on.<br /><br />Example:<br /><br />node/1<br />node/2<br />resources/*'),
    '#size' => 60,
    '#maxlength' => 500,
    '#wysiwyg' => TRUE,
    '#format' => 'full_html',
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['cancel_button'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('_brand_form_cancel'),
    '#limit_validation_errors' => array(),
  );

  $form['#pre_render'][] = 'vertical_tabs_form_pre_render';

  return $form;
}

/**
 * Implements hook_form().
 *
 * @inheritdoc
 */
function _brand_add_form($form, $form_state) {
  return array_merge($form, _brand_get_universal_form());
}

/**
 * Implements hook_form_submit().
 *
 * @inheritdoc
 */
function _brand_add_form_submit($form, $form_state) {

  global $user;
  $title = $form_state['values']['title'];
  $start = new DateTime($form_state['values']['date_start']['year'] . '-' . $form_state['values']['date_start']['month'] . '-' . $form_state['values']['date_start']['day']);
  $finish = new DateTime($form_state['values']['date_finish']['year'] . '-' . $form_state['values']['date_finish']['month'] . '-' . $form_state['values']['date_finish']['day']);
  if (module_exists('taxonomy')) {
    $term = (int) $form_state['values']['term'];
  }
  else {
    $term = 0;
  }
  $options = array(
    'title' => $form_state['values']['title'],
    'machine_name' => $form_state['values']['machine_name'],
    'description' => $form_state['values']['description'],
    'path_visibility' => $form_state['values']['paths'],
    'theme' => $form_state['values']['theme'],
    'weight' => $form_state['values']['weight'],
    'date_created' => $form_state['values']['date_created'],
    'date_start' => $start->getTimestamp(),
    'date_finish' => $finish->getTimestamp(),
    'date_lock' => $form_state['values']['date_lock'],
    'tid' => $term,
    'uid' => $user->uid,

    'roles' => brand_convert_roles($form_state['values']['roles']),
    'types' => brand_convert_types($form_state['values']['content_types']),
  );

  if (module_exists('book') && isset($form_state['values']['books'])) {
    $options['books'] = brand_convert_books($form_state['values']['books']);
  }

  brand_add($form_state['values']['machine_name'], $options);

  menu_rebuild();
  drupal_set_message("Sucessfully added brand {$title}", 'status');
  drupal_goto('/admin/config/user-interface/brands');
}

/**
 * Implements hook_form_validate().
 *
 * @inheritdoc
 */
function _brand_add_form_validate($form, $form_state) {
  $machine_name = strtolower($form_state['values']['machine_name']);
  if (FALSE !== strpos($form_state['values']['machine_name'], ' ')) {
    form_set_error('machine_name', t('Machine name cannot contain spaces.'));
  }
  $results = new Brand($machine_name, NULL);
  if (FALSE !== $results->raw()) {
    form_set_error('machine_name', t('Machine name is already taken.'));
  }
  if (FALSE !== strpos($form_state['values']['machine_name'], ' ')) {
    form_set_error('machine_name', t('Machine name cannot contain spaces.'));
  }
  if (FALSE !== strpos($form_state['values']['machine_name'], 'page')) {
    form_set_error('machine_name', t('Machine name cannot contain the term "page".'));
  }
  if (FALSE !== strpos($form_state['values']['machine_name'], 'node')) {
    form_set_error('machine_name', t('Machine name cannot contain the term "node".'));
  }
}

/**
 * Implements hook_form().
 *
 * @inheritdoc
 */
function _brand_delete_form($form, $form_state) {
  $arguments = explode('/', $_GET['q']);
  if (count($arguments) >= 4) {
    $machine_name = $arguments[4];
    if (count($arguments) >= 6 && $arguments[5] === 'delete') {
      $timestamp = $arguments[5];
    }

    $form['disclaimer'] = array(
      '#type' => 'markup',
    );
    if (isset($timestamp)) {
      $form['disclaimer']['#markup'] = "<p>Are you absolutely sure you wish to proceed with the following?</p><ul><li>Delete record of the brand identified by machine name '{$machine_name}' with matching creation timestamp of '{$timestamp}'</li></li></ul><p>This action is not reversible.</p></p>";
    }
    else {
      $form['disclaimer']['#markup'] = "<p>Are you absolutely sure you wish to proceed with the following?</p><ul><li>Delete all records of the brand identified by machine name '{$machine_name}'</li></li></ul><p>This action is not reversible.</p></p>";
    }
    $form['submit']['no'] = array(
      '#type' => 'submit',
      '#default_value' => 'No',
      '#submit' => array('_brand_form_delete_item_cancel'),
      '#limit_validation_errors' => array(),
    );
    $form['submit']['yes'] = array(
      '#type' => 'submit',
      '#default_value' => 'Yes',
    );
  }
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * @inheritdoc
 */
function _brand_delete_form_submit($form, $form_state) {
  $arguments = explode('/', $_GET['q']);
  $machine_name = $arguments[4];
  if (count($arguments) >= 7 && $arguments[6] === 'delete') {
    $timestamp = $arguments[5];
    brand_remove($machine_name, $timestamp);
    drupal_goto("/admin/config/user-interface/brands/{$machine_name}");
  }
  else {
    brand_remove($machine_name, NULL);
    drupal_goto('/admin/config/user-interface/brands/');
  }
}

/**
 * Cancel button for delete item form.
 */
function _brand_form_delete_item_cancel() {
  $arguments = explode('/', $_GET['q']);
  $machine_name = $arguments[4];
  if (is_numeric((int) $arguments[5])) {
    $timestamp = (int) $arguments[5];
  }
  if (isset($timestamp, $machine_name)) {
    drupal_goto("/admin/config/user-interface/brands/{$machine_name}/{$timestamp}");
  }
}

/**
 * Implements hook_form().
 *
 * @inheritdoc
 */
function _brand_edit_form($form, $form_state) {
  $arguments = explode('/', request_uri());
  if (count($arguments) === 7 && $arguments[4] === 'brands') {
    $machine_name = $arguments[5];
  }
  else {
    $arguments = explode('/', $_SERVER['HTTP_REFERER']);
    $machine_name = $arguments[7];
  }
  $brand = brand_load($machine_name);
  $terms = brand_form_get_terms();
  $start_date = getdate($brand->date_start);
  $finish_date = getdate($brand->date_finish);
  global $user;

  $form = _brand_get_universal_form();
  $form['basic']['title']['#default_value'] = $brand->title;
  $form['basic']['description']['#default_value'] = "Edited by {$user->name}";
  $form['basic']['machine_name']['#default_value'] = $brand->machine_name;
  $form['basic']['machine_name']['#disabled'] = TRUE;
  $form['basic']['machine_name']['#access'] = FALSE;
  $form['assets']['theme']['#default_value'] = $brand->theme;
  $form['dates']['date_start']['#default_value'] = array(
    'year' => $start_date['year'],
    'month' => $start_date['mon'],
    'day' => $start_date['mday'],
  );
  $form['dates']['date_finish']['#default_value'] = array(
    'year' => $finish_date['year'],
    'month' => $finish_date['mon'],
    'day' => $finish_date['mday'],
  );
  $form['dates']['date_lock']['#default_value'] = $brand->date_lock;
  $form['taxonomy']['term']['#options'] = $terms;
  $form['taxonomy']['term']['#default_value'] = $brand->tid;
  $form['visibility']['paths']['#default_value'] = $brand->path_visibility;

  $form['book']['books']['#default_value'] = $brand->books;
  $form['role']['roles']['#default_value'] = $brand->roles;
  $form['types']['content_types']['#default_value'] = $brand->types;

  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * @inheritdoc
 */
function _brand_edit_form_submit($form, $form_state) {
  global $user;
  $start_date = new DateTime($form_state['values']['date_start']['year'] . '-' . $form_state['values']['date_start']['month'] . '-' . $form_state['values']['date_start']['day']);
  $finish_date = new DateTime($form_state['values']['date_finish']['year'] . '-' . $form_state['values']['date_finish']['month'] . '-' . $form_state['values']['date_finish']['day']);
  $title = $form_state['values']['title'];
  $machine_name = $form_state['values']['machine_name'];
  if (module_exists('taxonomy')) {
    $term = (int) $form_state['values']['term'];
  }
  else {
    $term = 0;
  }
  $options = array(
    'title' => $form_state['values']['title'],
    'machine_name' => $form_state['values']['machine_name'],
    'description' => $form_state['values']['description'],
    'theme' => $form_state['values']['theme'],
    'weight' => $form_state['values']['weight'],
    'date_created' => $form_state['values']['date_created'],
    'date_start' => $start_date->getTimestamp(),
    'date_finish' => $finish_date->getTimestamp(),
    'date_lock' => $form_state['values']['date_lock'],
    'tid' => $term,
    'uid' => $user->uid,
    'path_visibility' => $form_state['values']['paths'],

    'roles' => brand_convert_roles($form_state['values']['roles']),
    'types' => brand_convert_types($form_state['values']['content_types']),
  );

  if (module_exists('book') && isset($form_state['values']['book'])) {
    $options['books'] = brand_convert_books($form_state['values']['books']);
  }

  brand_add($form_state['values']['machine_name'], $options);

  drupal_set_message("Sucessfully updated brand {$title}", 'status');
  drupal_goto("/admin/config/user-interface/brands/{$machine_name}");
}

/**
 * Implements hook_form_validate().
 *
 * @inheritdoc
 */
function _brand_edit_form_validate($form, $form_state) {
  // @TODO: Find out if nothing has changed, and report a form error if needed.
}

/**
 * Implements hook_form().
 *
 * @inheritdoc
 */
function _brand_view_form($form, $form_state) {
  $arguments = explode('/', $_GET['q']);
  $machine_name = $arguments[4];
  $timestamp = $arguments[5];
  $brand = brand_load($machine_name, $timestamp);
  if ($brand === NULL) {
    drupal_goto('/admin/config/user-interface/brands');
  }
  $form = array_merge($form, _brand_get_universal_form());
  $term = taxonomy_term_load($brand->tid);

  $form['basic']['title']['#default_value'] = $brand->title;
  $form['basic']['machine_name']['#default_value'] = $brand->machine_name;
  $form['basic']['description']['#default_value'] = $brand->description;
  $form['assets']['theme']['#default_value'] = $brand->theme;
  $form['assets']['weight']['#default_value'] = $brand->weight;
  $start_date = getdate($brand->date_start);
  $form['dates']['date_start']['#default_value'] = array(
    'year' => $start_date['year'],
    'month' => $start_date['mon'],
    'day' => $start_date['mday'],
  );
  $finish_date = getdate($brand->date_finish);
  $form['dates']['date_finish']['#default_value'] = array(
    'year' => $finish_date['year'],
    'month' => $finish_date['mon'],
    'day' => $finish_date['mday'],
  );
  $form['dates']['date_lock']['#default_value'] = $brand->date_lock;
  if ((int) $brand->tid > 0) {
    $form['taxonomy']['term']['#options'] = array(
      $term->tid => $term->name,
    );
    $form['taxonomy']['term']['#default_value'] = $term->name;
  }
  $form['visibility']['paths']['#default_value'] = $brand->path_visibility;

  $form['book']['books']['#default_value'] = $brand->books;
  $form['role']['roles']['#default_value'] = $brand->roles;
  $form['types']['content_types']['#default_value'] = $brand->types;

  $form['basic']['title']['#disabled'] = TRUE;
  $form['basic']['machine_name']['#disabled'] = TRUE;
  $form['basic']['machine_name']['#access'] = FALSE;
  $form['basic']['description']['#disabled'] = TRUE;

  $form['types']['type']['#disabled'] = TRUE;
  $form['assets']['theme']['#disabled'] = TRUE;
  $form['assets']['weight']['#disabled'] = TRUE;

  $form['dates']['date_created']['#disabled'] = TRUE;
  $form['dates']['date_start']['#disabled'] = TRUE;
  $form['dates']['date_finish']['#disabled'] = TRUE;
  $form['dates']['date_lock']['#disabled'] = TRUE;

  $form['taxonomy']['term']['#disabled'] = TRUE;
  $form['visibility']['paths']['#disabled'] = TRUE;

  $form['book']['books']['#disabled'] = TRUE;
  $form['role']['roles']['#disabled'] = TRUE;
  $form['types']['content_types']['#disabled'] = TRUE;

  $form['delete_button'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('_brand_form_delete_item'),
    '#limit_validation_errors' => array(),
  );

  unset($form['submit_button']);

  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * @inheritdoc
 */
function _brand_view_form_submit($form, $form_state) {

}

/**
 * Implements hook_form_validate().
 *
 * @inheritdoc
 */
function _brand_view_form_validate($form, $form_state) {

}

/**
 * Delete button for brand item.
 */
function _brand_form_delete_item() {
  $arguments = explode('/', $_GET['q']);
  $machine_name = $arguments[4];
  if (is_numeric((int) $arguments[5])) {
    $timestamp = (int) $arguments[5];
    drupal_goto("/admin/config/user-interface/brands/{$machine_name}/{$timestamp}/delete");
  }
}

<?php

/**
 * @file
 * Include file for all admin functions.
 */

/**
 * Remove submit function.
 */
function _brand_admin_settings_form_remove($form, &$form_state) {
  $delta_remove = $form_state['triggering_element']['#parents'][2];
  $k = array_search($delta_remove, $form_state['field_deltas']);
  unset($form_state['field_deltas'][$k]);

  $form_state['rebuild'] = TRUE;
  drupal_get_messages();
}

/**
 * Remove callback.
 */
function _brand_admin_settings_form_callback($form, &$form_state) {
  return $form['field_container']['themes'];
}

/**
 * Add submit function.
 */
function _brand_admin_settings_form_add($form, &$form_state) {
  $form_state['field_deltas'][] = count($form_state['field_deltas']) > 1 ? max($form_state['field_deltas']) + 1 : 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Admin form submit function callback.
 */
function _brand_admin_settings_form_submit($form, $form_state) {
  $inputs = $form_state['values']['field_container']['themes'];
  $dd = $form_state['values']['field_container']['themes']['disabled_themes'];
  $results = array();
  foreach ($inputs as $input) {
    if (isset($input['theme'])) {
      $results[] = $input['theme'];
    }
  }
  variable_set('brand_allowed_themes', $results);
  variable_set('brand_disable_checking', $dd);

  $message_enabled = $form_state['values']['field_container']['messages']['message_enabled'];
  $message_string = $form_state['values']['field_container']['messages']['message_string'];
  variable_set('brand_message_enabled', $message_enabled);
  variable_set('brand_message_string', $message_string);
}

/**
 * Admin form.
 */
function _brand_admin_settings_form($form, &$form_state) {
  $form['field_container'] = [
    '#type' => 'container',
    '#weight' => 80,
    '#tree' => TRUE,
  ];

  $form['field_container']['messages']['message_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable conflict message?'),
    '#description' => t('Enable warning messages when multiple brands are detected?'),
    '#default_value' => variable_get('brand_message_enabled', 0),
  ];

  $form['field_container']['messages']['message_string'] = [
    '#type' => 'textarea',
    '#title' => t('Message text.'),
    '#description' => t("The template message to use when multiple brands are detected for use.<br /><ul><li>You can use '[[brand]]' to replace that text with the machine name being used.</li><li>You can use '[[brands]]' to replace that text with all detected machine names for use.</li></ul>"),
    '#default_value' => variable_get('brand_message_string', "Multiple Brands have detected for use, the current brand in use uses the machine name [[brand]].<br />It was selected from [[brands]] by sorting the weight first, and then by alphabetical sorting.<br />If this is intentional, you can ignore this message."),
  ];

  // Get current values and quickly sanitize the indexes.
  $values = variable_get('brand_allowed_themes', array());
  $processed_values = array();
  foreach ($values as $value) {
    $processed_values[] = $value;
  }
  if (count($processed_values) - 1 < 0) {
    $count = 0;
  }
  else {
    $count = count($processed_values) - 1;
  }

  $form_state['field_deltas'] = isset($form_state['field_deltas']) ? $form_state['field_deltas'] : range(0, $count);
  $_themes = list_themes();
  $themes = array('' => '');
  foreach ($_themes as $theme) {
    $themes[$theme->name] = $theme->name;
  }

  $form['field_container']['themes'] = [
    '#title' => 'Allowed themes',
    '#description' => t('Themes configured for allowed use on this module are declared below.'),
    '#type' => 'fieldset',
    '#prefix' => '<div id="js-ajax-elements-wrapper">',
    '#suffix' => '</div>',
  ];

  $form['field_container']['themes']['disabled_themes'] = [
    '#type' => 'checkbox',
    '#title' => t('Disable theme control'),
    '#description' => t('Disable the mechanism which restricts the available themes.'),
    '#default_value' => variable_get('brand_disable_checking', 0),
  ];

  foreach ($form_state['field_deltas'] as $delta) {

    $field_value = isset($processed_values[$delta]) ? $processed_values[$delta] : NULL;

    $form['field_container']['themes'][$delta] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#tree' => TRUE,
    ];

    $form['field_container']['themes'][$delta]['theme'] = [
      '#type' => 'select',
      '#title' => check_plain('Allowed theme #' . $delta),
      '#default_value' => NULL,
      '#options' => $themes,
    ];

    if (isset($themes[$field_value])) {
      $form['field_container']['themes'][$delta]['theme']['#default_value'] = $themes[$field_value];
    }

    $form['field_container']['themes'][$delta]['remove'] = [
      '#type' => 'submit',
      '#default_value' => t('Remove'),
      '#submit' => ['_brand_admin_settings_form_remove'],
      '#ajax' => [
        'callback' => '_brand_admin_settings_form_callback',
        'wrapper' => 'js-ajax-elements-wrapper',
      ],
      '#weight' => 50,
      '#attributes' => [
        'class' => ['button-small'],
      ],
      '#name' => 'remove_theme_' . $delta,
    ];
  }

  // Submit buttons.
  $form['field_container']['themes']['add'] = [
    '#type' => 'submit',
    '#default_value' => t('Add one more'),
    '#submit' => ['_brand_admin_settings_form_add'],
    '#ajax' => [
      'callback' => '_brand_admin_settings_form_callback',
      'wrapper' => 'js-ajax-elements-wrapper',
    ],
    '#weight' => 100,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#default_value' => t('Save settings'),
    '#submit' => ['_brand_admin_settings_form_submit'],
    '#weight' => 150,
  ];

  return $form;
}

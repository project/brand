CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Brand module is an alternative to ThemeKey which focuses on change
management and access control. It uses a white-list to allow specific themes
under what is allowed to be managed via the module and provides full change
record management for independent entities managed by the console.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/brand

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/brand

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://drupal.org/project/views)

RECOMMENDED MODULES
-------------------

 * Book:
   When enabled, you can choose books for the system to target.
 * Taxonomy:
   When enabled, you'll be able to choose terms for the system to target.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Access and modify brand settings

     The administrative permission associated to access control for the
     administrative form. This form allows a user with the permission to
     manipulate and control the conflict messages. It also allows a user with
     the permission to add themes into a white-list for users to assign to
     each Brand, or to disable the white-list feature all-together.

   - Create new brands and brand entries

     In order for a user to create a given entry to a brand - new or otherwise,
     they must have this permission. Fundamentally this will allow them to add
     a database row, and to pick and choose the settings that go into a given
     Brand.

   - Remove brands and brand entries

     In order for a user to delete any Brand or Brand entry, this permission
     will need to be assigned. This role allows a user to delete a row from the
     database table - which will not be reflected in any form of change
     management provided by the module. It is not recommended to give this to
     anybody unless there's a specific need.

   - View brands and brand entries

     For a user to view a record, or the views integrated pages, this permission
     will need to be granted. It allows a given role to view the specifics of
     a Brand, including the path, the books and the theme name among other
     properties.

 * Customize the brand module settings in
    » Administration » Configuration » User Interface » Brands » Settings.

TROUBLESHOOTING
---------------

 * If the menu does not display, check the following:

   - Verify your user role matches up with the permissions listed above.

   - Clear your menu caches.

FAQ
---

There are currently no frequently asked questions, so please submit an issue
with your questions so we can add them here.

MAINTAINERS
-----------

Current maintainers:
 * Karl Hepworth - https://drupal.org/user/2190626

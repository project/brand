<?php

/**
 * @file
 * Installation file for the Brand module.
 */

/**
 * Implements hook_install().
 *
 * @inheritdoc
 */
function brand_install() {
  $t = get_t();
  drupal_set_message($t("Brand settings are available under !link",
    ['!link' => l($t('Administer > Site configuration > User Interface > Brands > Settings'), 'admin/config/user-interface/brands/settings')]
  ));
  if (NULL === variable_get('brand_allowed_themes', NULL)) {
    variable_set('brand_allowed_themes', array());
  }
  if (0 === variable_get('brand_allowed_themes', 0)) {
    variable_set('brand_disable_checking', 0);
  }
  $user_roles = user_roles();
  foreach ($user_roles as $role_id => $user_role) {
    if ($user_role === 'administrator') {
      user_role_grant_permissions($role_id, array(
        'brand settings',
        'brand creation',
        'brand removal',
        'brand visibility',
      ));
    }
  }
  drupal_flush_all_caches();
}

/**
 * Implements hook_uninstall().
 *
 * @inheritdoc
 */
function brand_uninstall() {
  db_drop_table('brand');
  variable_del('brand_allowed_themes');
  variable_del('brand_disable_checking');
  variable_del('brand_allowed_themes');
  variable_del('brand_disable_checking');
}

/**
 * Implements hook_schema().
 *
 * @inheritdoc
 */
function brand_schema() {
  $schema['brand'] = array(
    'description' => 'Data associated to the brand module.',
    'fields' => array(
      'id' => array(
        'description' => 'Unique identifier for the row',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The human-readable name for the brand.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'machine_name' => array(
        'description' => 'The machine name for the brand.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A commit message which describes this change.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'date_created' => array(
        'description' => 'UNIX timestamp of when this row was created.',
        'type' => 'int',
        'size' => 'big',
        'not null' => TRUE,
        'default' => 0,
      ),
      'date_lock' => array(
        'description' => 'Ignore date expiration.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'date_start' => array(
        'description' => 'UNIX timestamp of the start time/date.',
        'type' => 'int',
        'size' => 'big',
        'not null' => FALSE,
        'default' => 0,
      ),
      'date_finish' => array(
        'description' => 'UNIX timestamp of the finish time/date.',
        'type' => 'int',
        'size' => 'big',
        'not null' => FALSE,
        'default' => 0,
      ),
      'theme' => array(
        'description' => 'The theme to use.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => 'The weight for this brand.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'path_visibility' => array(
        'description' => 'A list of targets manually selected to inherit functionality.',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'tid' => array(
        'description' => 'The configured term ID.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'The configured user ID.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'books' => array(
        'description' => 'A serialized array of Books.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'roles' => array(
        'description' => 'A serialized array of Roles.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'types' => array(
        'description' => 'A serialized array of Types.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/**
 * The configured content type for the brand to use.
 */
function brand_update_7001() {
  $spec = array(
    'description' => 'The configured content type for the brand to use.',
    'type' => 'varchar',
    'length' => '255',
    'not null' => TRUE,
    'default' => '',
  );
  if (!db_field_exists('brand', 'content_type')) {
    db_add_field('brand', 'content_type', $spec);
  }
}

/**
 * Disable the new theme control mechanism.
 */
function brand_update_7002() {
  variable_set('brand_disable_checking', 1);
}

/**
 * Removes the VID field.
 */
function brand_update_7003() {
  if (db_field_exists('brand', 'vid')) {
    db_drop_field('brand', 'vid');
  }
}

/**
 * Creates new fields for books, roles and types.
 */
function brand_update_7010() {
  $books = array(
    'description' => 'A serialized array of Books.',
    'type' => 'text',
    'size' => 'big',
    'not null' => FALSE,
  );
  $roles = array(
    'description' => 'A serialized array of Roles.',
    'type' => 'text',
    'size' => 'big',
    'not null' => FALSE,
  );
  $types = array(
    'description' => 'A serialized array of Types.',
    'type' => 'text',
    'size' => 'big',
    'not null' => FALSE,
  );
  if (!db_field_exists('brand', 'books')) {
    db_add_field('brand', 'books', $books);
    $brands = brand_load_multiple();
    foreach ($brands as $brand) {
      $process = array();
      $process[$brand->bid] = (string) $brand->bid;
      $new = brand_convert_books($process);
      db_update('brand')
        ->condition('machine_name', $brand->machine_name, '=')
        ->condition('date_created', $brand->date_created, '=')
        ->fields(array(
          'books' => serialize($new),
        ))
        ->execute();
    }
  }
  if (!db_field_exists('brand', 'roles')) {
    db_add_field('brand', 'roles', $roles);
    $brands = brand_load_multiple();
    foreach ($brands as $brand) {
      $process = array();
      $process[$brand->rid] = (string) $brand->rid;
      $new = brand_convert_roles($process);
      db_update('brand')
        ->condition('machine_name', $brand->machine_name, '=')
        ->condition('date_created', $brand->date_created, '=')
        ->fields(array(
          'roles' => serialize($new),
        ))
        ->execute();
    }
  }
  if (!db_field_exists('brand', 'types')) {
    db_add_field('brand', 'types', $types);
    $brands = brand_load_multiple();
    foreach ($brands as $brand) {
      $process = array();
      $process[$brand->content_type] = (string) $brand->content_type;
      $new = brand_convert_types($process);
      db_update('brand')
        ->condition('machine_name', $brand->machine_name, '=')
        ->condition('date_created', $brand->date_created, '=')
        ->fields(array(
          'types' => serialize($new),
        ))
        ->execute();
    }
  }
}

/**
 * Removes single-value columns for BID, RID and Content type.
 */
function brand_update_7011() {
  if (db_field_exists('brand', 'bid')) {
    db_drop_field('brand', 'bid');
  }
  if (db_field_exists('brand', 'content_type')) {
    db_drop_field('brand', 'content_type');
  }
  if (db_field_exists('brand', 'rid')) {
    db_drop_field('brand', 'rid');
  }
}

<?php

/**
 * @file
 * Class file for the Brand class.
 */

/**
 * Brand is the class which contains the data for a given Brand.
 */
class Brand implements BrandInterface {

  /**
   * Raw contains the Raw value from the query before processing.
   *
   * @var object
   *   The raw value.
   */
  protected $Raw;
  /**
   * Brand contains the structured data for the Brand.
   *
   * @var object
   *   The data for the Brand.
   */
  protected $Brand;

  /**
   * Constructor.
   *
   * @inheritdoc
   */
  public function __construct(string $machine_name, int $timestamp = NULL, $skip_query = FALSE, $options = NULL) {

    // If the query has already run via Brands, we don't want to re-run it.
    if ($skip_query === TRUE) {
      if (NULL !== $options) {
        $obj = $options;
      }
      else {
        $obj = (object) array(
          'title' => '',
          'machine_name' => $machine_name,
          'description' => '',
          'date_created' => 0,
          'date_lock' => 0,
          'date_start' => 0,
          'date_finish' => 0,
          'path_visibility' => '',
          'theme' => '',
          'weight' => 0,
          'tid' => 0,
          'uid' => 0,

          'books' => serialize(array()),
          'roles' => serialize(array()),
          'types' => serialize(array()),
        );
      }
    }
    else {

      // We are tightly managing the schema through processing and
      // sensible defaults, and it's pretty stable.
      // Let's get all the fields so we can apply it.
      $data = db_select('brand', 'b');
      $data->fields('b');
      if (NULL !== $timestamp) {
        $data->condition('date_created', $timestamp, '=');
      }

      // Add the machine name as a condition and sort it.
      if ($machine_name !== '') {
        $data->condition('machine_name', $machine_name, '=');
      }

      $data->orderBy('b.date_created', 'ASC');

      // Execute and store it!
      $dataset = $data->execute()->fetchAll();

      $this->Raw = end($dataset);
      unset($dataset);

      // Return FALSE if nothing came back.
      if (empty($this->Raw)) {
        $this->Brand = NULL;
        return;
      }
      else {
        // Otherwise turn it into a structured array.
        $obj = (object) array(
          'title' => $this->Raw->title,
          'machine_name' => $this->Raw->machine_name,
          'description' => $this->Raw->description,
          'date_created' => $this->Raw->date_created,
          'date_lock' => $this->Raw->date_lock,
          'date_start' => $this->Raw->date_start,
          'date_finish' => $this->Raw->date_finish,
          'path_visibility' => $this->Raw->path_visibility,
          'theme' => $this->Raw->theme,
          'weight' => $this->Raw->weight,
          'tid' => $this->Raw->tid,
          'uid' => $this->Raw->uid,

          'books' => @unserialize($this->Raw->books),
          'roles' => @unserialize($this->Raw->roles),
          'types' => @unserialize($this->Raw->types),
        );
      }

    }
    $this->Brand = $obj;
  }

  /**
   * Check the brand to see if it's supposed to show.
   *
   * @inheritdoc
   */
  public function check() {
    if (isset($this->Brand->roles) && !empty($this->Brand->roles)) {
      foreach ($this->Brand->roles as $role) {
        if (user_has_role($role)) {
          if ((time() >= (int) $this->Brand->date_start && time() <= $this->Brand->date_finish) || (int) $this->Brand->date_lock === 1) {
            return TRUE;
          }
        }
      }
    }
    else {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Create a new database row based upon the current object.
   *
   * @inheritdoc
   */
  public function add(string $machine_name, array $options = array()) {
    if (user_access('brand creation')) {
      $query = db_insert('brand');
      $now = new DateTime();
      $title = (isset($options['title'])) ? $options['title'] : '';
      $description = (isset($options['description'])) ? $options['description'] : '';
      $date_created = $now->getTimestamp();
      $date_lock = (isset($options['date_lock'])) ? $options['date_lock'] : 0;
      $date_start = (isset($options['date_start'])) ? $options['date_start'] : $now->getTimestamp();
      $date_finish = (isset($options['date_finish'])) ? $options['date_finish'] : $now->getTimestamp();
      $path_visibility = (isset($options['path_visibility'])) ? $options['path_visibility'] : '';
      $theme = (isset($options['theme'])) ? $options['theme'] : '';
      $weight = (isset($options['weight'])) ? $options['weight'] : 0;
      $tid = (isset($options['tid'])) ? $options['tid'] : 0;
      $uid = (isset($options['uid'])) ? $options['uid'] : 0;

      $books = (isset($options['books'])) ? $options['books'] : serialize(array());
      $roles = (isset($options['roles'])) ? $options['roles'] : serialize(array());
      $types = (isset($options['types'])) ? $options['types'] : serialize(array());

      $mapped_fields = array(
        'title' => $title,
        'machine_name' => $machine_name,
        'description' => $description,
        'date_created' => $date_created,
        'date_lock' => $date_lock,
        'date_start' => $date_start,
        'date_finish' => $date_finish,
        'path_visibility' => $path_visibility,
        'theme' => $theme,
        'weight' => $weight,
        'tid' => $tid,
        'uid' => $uid,

        'books' => serialize($books),
        'roles' => serialize($roles),
        'types' => serialize($types),
      );

      $query->fields($mapped_fields);
      $query->execute();
      $this->Brand = (object) $mapped_fields;
    }
  }

  /**
   * Remove a database row based upon the current object.
   *
   * @inheritdoc
   */
  public function remove(int $timestamp = NULL) {
    if (user_access('brand removal')) {
      $machine_name = $this->Brand->machine_name;

      $q = db_delete('brand');
      $q->condition('machine_name', $machine_name, '=');
      if ($timestamp !== NULL) {
        $q->condition('date_created', $timestamp, '=');
        $q->execute();
        drupal_set_message(t("The record timestamped %time for brand %mname has been removed.", array('%time' => $timestamp, '%mname' => $machine_name)));
        drupal_goto("/admin/config/user-interface/brands/{$machine_name}");
      }
      else {
        $q->execute();
        drupal_set_message(t("The brand %mname has been removed.", array('%mname' => $machine_name)));
        drupal_goto("/admin/config/user-interface/brands/");
      }
    }
  }

  /**
   * Returns the $Brand object for code compliance checks.
   *
   * @return \stdClass
   *   The Brand object containing the data.
   */
  public function get() {
    return $this->Brand;
  }

  /**
   * Returns the $Raw object for code compliance checks.
   *
   * @return \stdClass
   *   The Raw object containing the data.
   */
  public function raw() {
    return $this->Raw;
  }

}

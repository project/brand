<?php

/**
 * @file
 * Handler file for the active state.
 */

/**
 * Provide the active state of the brand's row.
 *
 * @ingroup views_filter_handlers
 */
class BrandHandlerActiveStatus extends views_handler_field {

  /**
   * Removes the query associated to this handler.
   *
   * @inheritdoc
   */
  public function query() {
  }

  /**
   * Render the active state of the brand.
   *
   * @inheritdoc
   */
  public function render($values) {
    $brand = brand_load($values->brand_machine_name, $values->brand_date_created);
    if (NULL !== $brand->theme && $brand->theme !== '' && $brand->theme !== 'none') {
      $check = brand_check($values->brand_machine_name, $values->brand_date_created);

      if ($check === TRUE) {
        return 'Allowed';
      }
      if ($check === FALSE) {
        return 'Not allowed';
      }
    }
    elseif ($brand->theme === 'none') {
      return 'Not configured';
    }
    return 'Unknown';
  }

}

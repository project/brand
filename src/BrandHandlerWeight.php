<?php

/**
 * @file
 * Handler file for the weight field.
 */

/**
 * Provide the first name only from the name field.
 *
 * @ingroup views_filter_handlers
 */
class BrandHandlerWeight extends views_handler_field {

  /**
   * Render the weight field.
   */
  public function render($values) {
    return $values->brand_weight;
  }

}

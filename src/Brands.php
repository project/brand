<?php

/**
 * @file
 * Class file for the Brands class.
 */

/**
 * Class Brands contains a simple way to store and fetch multiple Brands.
 */
class Brands implements BrandsInterface {

  /**
   * Structured array of Brands based upon input parameters.
   *
   * @var object
   *   The data for the array of Brands.
   */
  protected $Brands;

  /**
   * The constructor function for a Brands object.
   *
   * @inheritdoc
   */
  public function __construct(string $machine_name = NULL, bool $latest_only = NULL) {
    $data = db_select('brand', 'n')
      ->fields('n');

    // If a machine name was specified, add it as a condition.
    if (NULL !== $machine_name) {
      $data->condition('machine_name', $machine_name, '=');
    }

    if (!empty($latest_only)) {
      // @see brand_query_alter.
      $data->addTag('latest_revisions_only');
    }

    // Sort it.
    $data->orderBy('n.date_created', 'ASC');

    // Execute and store it!
    $rawdata = $data->execute()->fetchAll();
    $brands = array();
    foreach ($rawdata as $dataset) {
      $brands[] = new Brand($dataset->machine_name, $dataset->date_created, TRUE, $dataset);
    }

    $this->Brands = $brands;
  }

  /**
   * Returns the data structure in a predictable fashion.
   *
   * @inheritdoc
   */
  public function get() {
    $results = array();
    if (isset($this->Brands)) {
      foreach ($this->Brands as $brand) {
        $results[] = $brand->get();
      }
    }
    return $results;
  }

}

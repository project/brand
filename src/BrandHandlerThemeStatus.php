<?php

/**
 * @file
 * Handler file for the theme status.
 */

/**
 * Provide the active state of the brand's row.
 *
 * @ingroup views_filter_handlers
 */
class BrandHandlerThemeStatus extends views_handler_field {

  /**
   * Empty the default query to produce a custom value.
   *
   * @inheritdoc
   */
  public function query() {
  }

  /**
   * Render the active state of the brand.
   *
   * @inheritdoc
   */
  public function render($values) {
    $brand = brand_load($values->brand_machine_name, $values->brand_date_created);
    $theme = $brand->theme;
    $themes = list_themes();
    if ($theme !== 'none') {
      $allowed_themes = variable_get('brand_allowed_themes', array());
      foreach ($allowed_themes as $allowed_theme) {
        if (isset($themes[$theme]) && (int) $themes[$theme]->status === 1) {
          if ($theme === $allowed_theme) {
            return 'Allowed';
          }
          if (variable_get('brand_disable_checking', 0) === 1) {
            return 'Allowed';
          }
        }
        return 'Not allowed';
      }
      return 'Not configured';
    }
  }

}
